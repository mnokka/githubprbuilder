### GitHub PullRequest Bamboo builder ###

* Github PullRequest is initiating Bamboo build (for the originating branch)

####Detailed operations and info
    Github projet needs webhook setup (pull request information, data to be send to the flask server)
    Bamboo project linked to Github repo needs to have branch plans activated 
    Github must have incoming rule in firewall (development done with ngrok)
    
    When Github PR event is received, it's mathcing Bamboo build plan is digged from DB 
    Finally Bamboo is commanded to to build the matching build plan
    
    
####Requirements
    MySQL as Bamboo DB
    Python 2.7
    Use pip install -r requirements.txt to install needed libraries
    Recommending usage of Python virtual env
	Screen can be used to leave flask server running state 
	Testing behind firewall, ngrok usage recommended (before actual firewall rule creations)
    
    MySQL was a bit problematic library:
    MySQL library installation (RedHat)
        yum install mysql-devel python-devel
        sudo pip install MySQL-python (if virtualenv in use, install in venv)
        
    Mint 17.2(Ubuntu based) installation
      Used lots of web recipies to get it through
      1) https://www.tutorialspoint.com/python/python_database_access.htm
      These were used to get it all to work:
      2) sudo apt-get install build-essential python-dev libapache2-mod-wsgi-py3 libmysqlclient-dev
      3)  pip install MySQL-python
            
   Created own branch for PostgreSQL DB usage
    

####Authentication

    .netrc file used to provide logging credentials 
    Remember .netrc file protection!

####Modules
    GithubEventParser.py : Flask server to parse Github PullRqeust events
    SQLFinder.py: mySQL finder , finds Bamboo build vs Github branch relationship
    BambooCommander.py: Bamboo build starter


#### Contribution guidelines ###

* Via issues or fork+pr